import React, { Component } from 'react'
import DefaultLayout from "layouts/DefaultLayout";
import {createFragmentContainer, graphql} from "react-relay";

class Page extends Component {
    render() {
        return (
            <DefaultLayout>
                {this.props.page.title}
            </DefaultLayout>
        )
    }
}

export default createFragmentContainer(Page, {fragments: {
page: () => Relay.QL`
fragment on Page {
    title
    slug
    linkText
    }`}
}
)