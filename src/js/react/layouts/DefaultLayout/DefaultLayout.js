import React from 'react';
import environment from "../../environment";
const {
    QueryRenderer,
    graphql,
} = require('react-relay'); // or require('react-relay/compat') for compatibility
import LinkList from "../../components/Nav/LinkList";

class DefaultLayout extends React.Component {
    render() {
        return (
            <div id="app">
                <QueryRenderer
                    environment={environment}
                    query={graphql`
                    query DefaultLayoutQuery {
                      Pages {
                        id
                        slug
                        title
                        linkText
                      }
                    }
                  `}
                    render={({error, props}) => {
                        if (error) {
                            return <div>{error.message}</div>;
                        } else if (props) {
                            return <div><LinkList pages={props.Pages}/> is great!</div>;
                        }
                        return <div>Loading</div>;
                    }}
                />
                {this.props.children}
            </div>
        );
    }
}
module.exports = DefaultLayout;
