import React, { Component } from 'react';
import {
    createFragmentContainer,
    graphql
} from 'react-relay';

class Link extends Component {

    render() {
        return (
            <li>{this.props.page.linkText}</li>
        )
    }


}

export default createFragmentContainer(Link, {
    fragments: {
        page: () => Relay.QL`
  fragment on Page {
    id
    linkText
  }
`}
})