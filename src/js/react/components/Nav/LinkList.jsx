import React, { Component } from 'react'
import {
    createFragmentContainer,
    graphql
} from 'react-relay';
import {Link} from 'react-router-dom';

class LinkList extends Component {

    render() {
        return (
            <div>
                {this.props.pages.map(page => (
                    <Link to={'/'+page.slug} key={page.id} >{page.linkText}</Link>
                ))}
            </div>
        )
    }

}

export default createFragmentContainer(LinkList, graphql`
fragment LinkList_pages on Page @relay(plural: true) {
    id
    title
    slug
    linkText
  }`
)