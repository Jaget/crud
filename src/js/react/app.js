import React from "react";
import ReactDOM from "react-dom";
import Page from "pages/Page";
import {Relay} from 'react-relay';
import { BrowserRouter } from 'react-router-dom'
import { Switch, Route } from 'react-router';

if (document.querySelector("#app")) {
  ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path='/:slug' component={Page} queries={{page: () => Relay.QL`
            query {
                page(slug: $slug)
            }`}}/>
        </Switch>
    </BrowserRouter>,
    document.querySelector("#app")
  );
}
