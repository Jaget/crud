var React = require('react');

class DefaultLayout extends React.Component {
    render() {
        return (
            <html>
            <head>
                <title>{this.props.title}</title>
                <link rel="stylesheet" type="text/css" href="/style.css" />
            </head>
            <body>
                <div id="app">
                    <nav>
                    </nav>
                    {this.props.children}
                </div>
            </body>
            <script src="/bundle.js" />
            </html>
        );
    }
}

module.exports = DefaultLayout;