import Sequelize from "sequelize";

import Connection from "./connection";

var db = {
	Page: Connection.import("./models/Page.js")
};

db.sequelize = Connection;
db.Sequelize = Sequelize;

Connection.sync();

export default db;
