module.exports = function(sequelize, DataTypes) {
	const Page = sequelize.define("Page", {
		title: {
			type: DataTypes.STRING,
			allowNull: false
		},
		slug: {
			type: DataTypes.STRING,
			allowNull: false
		},
		linkText: {
			type: DataTypes.STRING,
			allowNull: true
		}
	});

	return Page;
};
