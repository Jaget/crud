import { Sequelize } from "sequelize";

const connection = new Sequelize("postgres", "docker", "docker", {
	dialect: "postgres",
	host: "localhost"
});

console.info("SETUP - Connecting database...");

connection
	.authenticate()
	.then(() => {
		console.info("INFO - Database connected.");
	})
	.catch(err => {
		console.error("ERROR - Unable to connect to the database:", err);
	});

export default connection;
