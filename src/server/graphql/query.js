import { GraphQLObjectType } from "graphql";

import Page from "./pageQuery";

const query = new GraphQLObjectType({
	name: "Query",
	fields: {
		...Page
	}
});

export default query;
