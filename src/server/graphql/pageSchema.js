import {
	GraphQLBoolean,
	GraphQLID,
	GraphQLInputObjectType,
	GraphQLInt,
	GraphQLList,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLString
} from "graphql";
import { resolver } from "graphql-sequelize";

import db from "../database/index";

export const Page = new GraphQLObjectType({
	name: "Page",
	description: "A page in the site",
	fields: () => ({
		id: {
			type: GraphQLID,
			description: "The userId provided by auth0"
		},
		title: {
			type: GraphQLString,
			description: "The pages title"
		},
		slug: {
			type: GraphQLString,
			description: "The pages slug value"
		},
		linkText: {
			type: GraphQLString,
			description: "Page link text for use in anchor tags"
		},
		createdAt: {
			type: GraphQLInt,
			description: "The timestamp when created"
		},
		updatedAt: {
			type: GraphQLInt,
			description: "The timestamp when last updated"
		}
	})
});
