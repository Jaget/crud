import { GraphQLObjectType } from "graphql";

import Page from "./models/Page/pageMutation";

const query = new GraphQLObjectType({
	name: "mutations",

	fields: {
		...Page
	}
});

export default query;
