import { resolver } from "graphql-sequelize";
import { GraphQLList, GraphQLString } from "graphql";

import db from "../database/index";
import { Page } from "./pageSchema";

export default {
	page: {
		type: Page,
		description: "A query to get pages",
		args: {
			slug: {type: GraphQLString}
		},
		resolve: resolver(db.Page)
	},
	Pages: {
		type: new GraphQLList(Page),
		description: "A query to get pages",
		resolve: resolver(db.Page)
	}
};
